import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const instance = axios.create({
  baseURL: 'http://localhost:4000/api',
});

Vue.prototype.$http = instance;

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
