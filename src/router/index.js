import Vue from 'vue';
import VueRouter from 'vue-router';
import Singin from '../views/Singin.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Singin',
    component: Singin,
  },
  {
    path: '/singup',
    name: 'Singup',
    component: () => import('../views/Singup.vue'),
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
